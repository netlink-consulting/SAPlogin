﻿/*
  SAPlogin

  Copyright (c) 2019 Bernhard W. Radermacher <bernhard.raderamcher@netlink-consulting.com>
*/

using System;
using System.Windows.Forms;
using KeePass.UI;


namespace SAPlogin
{
    sealed class MenuTree : ToolStripMenuItem
	{
		private readonly LoginOptions m_options;

		public MenuTree(LoginOptions options) : base("SAP Login") 
		{ 
			m_options = options;

			ToolStripMenuItem tsmiDisplaySID = new ToolStripMenuItem
			{
				Text = "Display SID"
			};
			tsmiDisplaySID.Click += this.OnMenuDisplaySID;
			this.DropDownItems.Add(tsmiDisplaySID);

			ToolStripMenuItem tsmiDisplayClient = new ToolStripMenuItem
			{
				Text = "Display Client"
			};
			tsmiDisplayClient.Click += this.OnMenuDisplayClient;
			this.DropDownItems.Add(tsmiDisplayClient);

			ToolStripMenuItem tsmiDisplayLanguage = new ToolStripMenuItem
			{
				Text = "Display Language"
			};
			tsmiDisplayLanguage.Click += this.OnMenuDisplayLanguage;
			this.DropDownItems.Add(tsmiDisplayLanguage);

			ToolStripMenuItem tsmiDisplayTransaction = new ToolStripMenuItem
			{
				Text = "Display Transaction"
			};
			tsmiDisplayTransaction.Click += this.OnMenuDisplayTransaction;
			this.DropDownItems.Add(tsmiDisplayTransaction);

			this.DropDownItems.Add(new ToolStripSeparator());

			ToolStripMenuItem tsmiMaximizeWindow = new ToolStripMenuItem
			{
				Text = "Maximize SAPgui"
			};
			tsmiMaximizeWindow.Click += this.OnMenuMaximizeWindow;
			this.DropDownItems.Add(tsmiMaximizeWindow);

			this.DropDownItems.Add(new ToolStripSeparator());

			ToolStripMenuItem tsmiLanguage1 = new ToolStripMenuItem("Default Language");
			this.DropDownItems.Add(tsmiLanguage1);

			ToolStripMenuItem tsmiLanguageEN = new ToolStripMenuItem
			{
				Text = "EN - English"
			};
			tsmiLanguageEN.Click += this.OnMenuLanguageEN;
			tsmiLanguage1.DropDownItems.Add(tsmiLanguageEN);

			ToolStripMenuItem tsmiLanguageDE = new ToolStripMenuItem
			{
				Text = "DE - German"
			};
			tsmiLanguageDE.Click += this.OnMenuLanguageDE;
			tsmiLanguage1.DropDownItems.Add(tsmiLanguageDE);

			ToolStripMenuItem tsmiLanguage2 = new ToolStripMenuItem("more...");
			tsmiLanguage1.DropDownItems.Add(tsmiLanguage2);

			ToolStripMenuItem tsmiLanguageFR = new ToolStripMenuItem
			{
				Text = "FR - French"
			};
			tsmiLanguageFR.Click += this.OnMenuLanguageFR;
			tsmiLanguage2.DropDownItems.Add(tsmiLanguageFR);

			ToolStripMenuItem tsmiLanguageES = new ToolStripMenuItem
			{
				Text = "ES - Spanish"
			};
			tsmiLanguageES.Click += this.OnMenuLanguageES;
			tsmiLanguage2.DropDownItems.Add(tsmiLanguageES);

			ToolStripMenuItem tsmiLanguageIT = new ToolStripMenuItem
			{
				Text = "IT - Italian"
			};
			tsmiLanguageIT.Click += this.OnMenuLanguageIT;
			tsmiLanguage2.DropDownItems.Add(tsmiLanguageIT);

			ToolStripMenuItem tsmiLanguageNL = new ToolStripMenuItem
			{
				Text = "NL - Dutch"
			};
			tsmiLanguageNL.Click += this.OnMenuLanguageNL;
			tsmiLanguage2.DropDownItems.Add(tsmiLanguageNL);

			ToolStripMenuItem tsmiLanguageJP = new ToolStripMenuItem
			{
				Text = "JP - Japanese"
			};
			tsmiLanguageJP.Click += this.OnMenuLanguageJP;
			tsmiLanguage2.DropDownItems.Add(tsmiLanguageJP);


			this.DropDownOpening += delegate (object sender, EventArgs e)
			{
				UIUtil.SetChecked(tsmiDisplaySID, m_options.DisplaySID);
				UIUtil.SetChecked(tsmiDisplayClient, m_options.DisplayClient);
				UIUtil.SetChecked(tsmiDisplayLanguage, m_options.DisplayLanguage);
				UIUtil.SetChecked(tsmiDisplayTransaction, m_options.DisplayTransaction);
				UIUtil.SetChecked(tsmiMaximizeWindow, m_options.MaximizeWindow);

				UIUtil.SetChecked(tsmiLanguageEN, LanguageChecked("EN"));
				UIUtil.SetChecked(tsmiLanguageDE, LanguageChecked("DE"));
				UIUtil.SetChecked(tsmiLanguageFR, LanguageChecked("FR"));
				UIUtil.SetChecked(tsmiLanguageES, LanguageChecked("ES"));
				UIUtil.SetChecked(tsmiLanguageIT, LanguageChecked("IT"));
				UIUtil.SetChecked(tsmiLanguageNL, LanguageChecked("NL"));
				UIUtil.SetChecked(tsmiLanguageJP, LanguageChecked("JP"));
			};

		}

		private void OnMenuDisplaySID(object sender, EventArgs e)
		{
			m_options.DisplaySID = !m_options.DisplaySID;
		}

		private void OnMenuDisplayClient(object sender, EventArgs e)
		{
			m_options.DisplayClient = !m_options.DisplayClient;
		}

		private void OnMenuDisplayLanguage(object sender, EventArgs e)
		{
			m_options.DisplayLanguage = !m_options.DisplayLanguage;
		}

		private void OnMenuDisplayTransaction(object sender, EventArgs e)
		{
			m_options.DisplayTransaction = !m_options.DisplayTransaction;
		}

		private void OnMenuMaximizeWindow(object sender, EventArgs e)
		{
			m_options.MaximizeWindow = !m_options.MaximizeWindow;
		}

		private void OnMenuLanguageEN(object sender, EventArgs e)
		{
			m_options.DefaultLanguage = "EN";
		}

		private void OnMenuLanguageDE(object sender, EventArgs e)
		{
			m_options.DefaultLanguage = "DE";
		}

		private void OnMenuLanguageFR(object sender, EventArgs e)
		{
			m_options.DefaultLanguage = "FR";
		}

		private void OnMenuLanguageES(object sender, EventArgs e)
		{
			m_options.DefaultLanguage = "ES";
		}

		private void OnMenuLanguageIT(object sender, EventArgs e)
		{
			m_options.DefaultLanguage = "IT";
		}

		private void OnMenuLanguageNL(object sender, EventArgs e)
		{
			m_options.DefaultLanguage = "NL";
		}

		private void OnMenuLanguageJP(object sender, EventArgs e)
		{
			m_options.DefaultLanguage = "JP";
		}

		private bool LanguageChecked(string Language)
		{
			if (Language == m_options.DefaultLanguage) return true;
			return false;
		}

	}

}
