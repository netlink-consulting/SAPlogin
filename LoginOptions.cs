﻿/*
  SAPlogin

  Copyright (c) 2019 Bernhard W. Radermacher <bernhard.raderamcher@netlink-consulting.com>
*/

using KeePass.Plugins;
using Microsoft.Win32;
using System;
using System.IO;

namespace SAPlogin
{
    public sealed class LoginOptions
    {
        private const string OptionDisplaySID = "SAPlogin_DisplaySID";
        private const string OptionDisplayClient = "SAPlogin_DisplayClient";
        private const string OptionDisplayLanguage = "SAPlogin_DisplayLanguage";
        private const string OptionDisplayTransaction = "SAPlogin_DisplayTransaction";
        private const string OptionMaximizeWindow = "SAPlogin_MaximizeWindow";
        private const string OptionDefaultLanguage = "SAPlogin_DefaultLanguage";

        private readonly IPluginHost m_host;

        private bool m_displaySID;
        private bool m_displayClient;
        private bool m_displayLanguage;
        private bool m_displayTransaction;
        public bool MaximizeWindow;
        public string DefaultLanguage;
        private string m_formatString;
        private string m_reducedFormatString;
        private readonly string m_sapshcut;

        public string Executable
        {
            get { return m_sapshcut; }
        }

        public bool DisplaySID
        {
            get { return m_displaySID; }
            set
            {
                m_displaySID = value;
                BuildFormatString();
            }
        }
        public bool DisplayClient
        {
            get
            {
                return m_displayClient;
            }
            set
            {
                m_displayClient = value;
                BuildFormatString();
            }
        }

        public bool DisplayLanguage
        {
            get
            {
                return m_displayLanguage;
            }
            set
            {
                m_displayLanguage = value;
                BuildFormatString();
            }
        }

        public bool DisplayTransaction
        {
            get
            {
                return m_displayTransaction;
            }
            set
            {
                m_displayTransaction = value;
                BuildFormatString();
            }
        }
        public string FormatString { get { return m_formatString; } }
        public string ReducedFormatString { get { return m_reducedFormatString; } }

        public LoginOptions(IPluginHost host)
        {
            m_host = host;
            m_displaySID = m_host.CustomConfig.GetBool(OptionDisplaySID, true);
            m_displayClient = m_host.CustomConfig.GetBool(OptionDisplayClient, true);
            m_displayLanguage = m_host.CustomConfig.GetBool(OptionDisplayLanguage, false);
            m_displayTransaction = m_host.CustomConfig.GetBool(OptionDisplayTransaction, false);
            MaximizeWindow = m_host.CustomConfig.GetBool(OptionMaximizeWindow, false);
            DefaultLanguage = m_host.CustomConfig.GetString(OptionDefaultLanguage, "EN");

            BuildFormatString();
            m_sapshcut = DetectSAPguiPath();
        }

        private void BuildFormatString()
        {
            m_formatString = "";

            if (m_displaySID) m_formatString = "{0}";
            if (m_displayClient)
            {
                if (!string.IsNullOrEmpty(FormatString)) m_formatString += "/";
                m_formatString += "{1}";
            }
            m_reducedFormatString = FormatString;
            if (m_displayLanguage)
            {
                if (!string.IsNullOrEmpty(FormatString)) m_formatString += " ";
                m_formatString += "[{2}]";
            }
            if (m_displayTransaction)
            {
                if (!string.IsNullOrEmpty(FormatString)) m_formatString += " - ";
                if (!string.IsNullOrEmpty(ReducedFormatString)) m_reducedFormatString += " - ";
                m_formatString += "{3}";
                m_reducedFormatString += "{3}";
            }
        }

        public void Terminate()
        {
            m_host.CustomConfig.SetBool(OptionDisplaySID, DisplaySID);
            m_host.CustomConfig.SetBool(OptionDisplayClient, DisplayClient);
            m_host.CustomConfig.SetBool(OptionDisplayLanguage, DisplayLanguage);
            m_host.CustomConfig.SetBool(OptionDisplayTransaction, DisplayTransaction);
            m_host.CustomConfig.SetBool(OptionMaximizeWindow, MaximizeWindow);
            m_host.CustomConfig.SetString(OptionDefaultLanguage, DefaultLanguage);
        }


        private string DetectSAPguiPath()
        {
            RegistryKey rootKey = RegistryKey.OpenRemoteBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, "");
            RegistryKey subKey;

            string sPath = "";
            string fileLoc;

            try
            {
                // Check path from registry for x86
                subKey = rootKey.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\sapshcut.exe");
                sPath = Convert.ToString(subKey.GetValue("Path"));
            }
            catch { }
 
            if (string.IsNullOrEmpty(sPath))
            {
                try
                {
                    // Check path from registry for 64bit
                    subKey = rootKey.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\App Paths\\sapshcut.exe");
                    sPath = Convert.ToString(subKey.GetValue("Path"));
                }
                catch { }
            }

            if (!string.IsNullOrEmpty(sPath))
            {
                fileLoc = Path.Combine(sPath, "sapshcut.exe");
                FileInfo fileInfo = new FileInfo(fileLoc);

                if (fileInfo.Exists) return fileLoc;
            }
            return "";

        } 
    }
}
