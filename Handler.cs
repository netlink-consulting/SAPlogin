﻿/*
  SAPlogin

  Copyright (c) 2019 Bernhard W. Radermacher <bernhard.raderamcher@netlink-consulting.com>
*/

using System.Diagnostics;
using System.Windows.Forms;
using KeePassLib.Security;

namespace SAPlogin
{
    public sealed class Handler
    {
        public static bool DoLogon(KeePassEntry entry, string language, ProtectedString user, ProtectedString password, LoginOptions options)
        {
            if (string.IsNullOrEmpty(options.Executable))
            {
                MessageBox.Show("SAPgui not installed!", "SAPgui", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            //Example:
            //
            // "C:\Program Files (x86)\SAP\FrontEnd\SAPgui\sapshcut" -system=ABC -client=010 -user=username -pw=pass -language=EN -maxgui -command=SE10

            string strArgs = string.Format("-system={0} -client={1} -user={2} -pw={3} -language={4}", 
                entry.SID, entry.Client, user.ReadString(), password.ReadString(), language);
            if (!string.IsNullOrEmpty(entry.Host)) strArgs += string.Format(" -guiparm=\"{0} {1}\"",
                entry.Host, entry.Instance);

            if (!string.IsNullOrEmpty(entry.Transaction)) strArgs += " -command=" + entry.Transaction;
            if (options.MaximizeWindow) strArgs += " -maxgui";

            ProcessStartInfo info = new ProcessStartInfo(options.Executable);
            info.Arguments = strArgs;
            info.CreateNoWindow = false;
            info.UseShellExecute = true;
            info.ErrorDialog = true;
            info.RedirectStandardInput = false;
            info.RedirectStandardOutput = false;

            Process process = Process.Start(info);

            return !(process.HasExited);
        }
    }
}