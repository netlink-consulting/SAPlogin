# SAPlogin

This KeePass Password Safe extension (plugin) enables you to logon to SAP systems (SAPgui) by simply clicking on the password entry.
See [KeePass Password Safe](https://keepass.info/).

_Inspired by [KeeSAPLogon](https://sourceforge.net/projects/keesaplogon/) by Marko Graf_ 

## How it works

Simply add custom strings at each KeePass entry you want to use for SAP Logon. These custom strings will be recognized by the plugin:

- SAP_SID
- SAP_Client
- SAP_Language _optional_
- SAP_Transaction _optional_

The following optional custom strings must be provided together: 
- SAP_Host
- SAP_Instance

**Note:** SAPgui must be installed.

### Prerequisites

- Windows 32/64bit
- KeePass 2.43 or newer
- SAPgui 7.30 or newer

### Features

- logon to SAP system (via SAPgui) by simple click on KeePass column
- separate columns to use EN (english) or DE (german)
- full integration into KeePass Password Safe (KeePass 2.xx)
- default language for all systems
- autodetection of SAPgui location
- handles references between KeePass entries
- maximize SAPgui window (configurable)

### Installation

Copy [SAPlogin.plgx](https://gitlab.com/netlink-consulting/SAPlogin/raw/master/SAPlogin.plgx) to your KeePass `Plugins` directory.
