﻿/*
  SAPlogin

  Copyright (c) 2019 Bernhard W. Radermacher <bernhard.raderamcher@netlink-consulting.com>
*/

using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

using KeePass.Forms;
using KeePass.Plugins;
using KeePass.Resources;
using KeePass.Util.Spr;
using KeePassLib;
using KeePassLib.Security;
using KeePassLib.Utility;
using KeePass.UI;
using System;
using Microsoft.Win32;

namespace SAPlogin
{
    public sealed class SAPloginExt : Plugin
    {
        private IPluginHost m_host;
        private LoginOptions m_options;
        private MenuTree m_menuTree;

        private LoginColumnProvider m_columnProvider;
        private LoginColumnProvider m_columnProviderDE;
        private LoginColumnProvider m_columnProviderEN;

        public override bool Initialize(IPluginHost host)
        {
            if (host == null) return false;

            m_host = host;
            m_options = new LoginOptions(m_host);
            m_menuTree = new MenuTree(m_options);

            m_columnProvider = new LoginColumnProvider(m_host, m_options);
            m_host.ColumnProviderPool.Add(m_columnProvider);
            m_columnProviderDE = new LoginColumnProvider(m_host, m_options, "DE");
            m_host.ColumnProviderPool.Add(m_columnProviderDE);
            m_columnProviderEN = new LoginColumnProvider(m_host, m_options, "EN");
            m_host.ColumnProviderPool.Add(m_columnProviderEN);

            return true;
        }

        public override void Terminate()
        {
            m_options.Terminate();
        }

        public override ToolStripMenuItem GetMenuItem(PluginMenuType t)
        {
            if (t != PluginMenuType.Main) return null;

            return m_menuTree;
        }

        public sealed class LoginColumnProvider : ColumnProvider
        {
            private readonly IPluginHost m_host;
            private readonly LoginOptions m_options;
            private readonly string m_language;
            private readonly string[] m_vColNames;

            public LoginColumnProvider(IPluginHost host, LoginOptions options, string language = "")
            {
                m_host = host;
                m_options = options;
                m_language = language.ToUpper();
                if (string.IsNullOrWhiteSpace(m_language)) m_vColNames = new string[] { "SAP Login" };
                else m_vColNames = new string[] { "[" + m_language + "] SAP" };

            }

            public override string[] ColumnNames
            {
                get { return m_vColNames; }
            }

            public override HorizontalAlignment TextAlign
            {
                get { return HorizontalAlignment.Left; }
            }

            public override string GetCellData(string strColumnName, PwEntry pe)
            {
                string strCellData = "n/a";
                string formatString = m_options.FormatString;
                if (!string.IsNullOrWhiteSpace(m_language)) formatString = m_options.ReducedFormatString;

                KeePassEntry entry = new KeePassEntry(pe, m_options.DefaultLanguage);

                if (entry.IsValid())
                {
                    strCellData = string.Format(formatString, entry.SID, entry.Client, entry.Language, entry.Transaction);
                }
                else
                {
                    if (entry.IsSAP()) strCellData = "error";
                }
                return strCellData;
            }

            public override bool SupportsCellAction(string strColumnName)
            {
                if (string.IsNullOrEmpty(strColumnName) || strColumnName != m_vColNames[0]) return false;

                return true;
            }

            public override void PerformCellAction(string strColumnName, PwEntry pe)
            {
                KeePassEntry entry = new KeePassEntry(pe, m_options.DefaultLanguage);

                if (entry.IsValid())
                {
                    ProtectedString pUser = DerefValue(PwDefs.UserNameField, pe);
                    ProtectedString pPw = DerefValue(PwDefs.PasswordField, pe);
                    string language = entry.Language;
                    if (!string.IsNullOrEmpty(m_language)) language = m_language;

                    Handler.DoLogon(entry, language, pUser, pPw, m_options);
                }
            }
        
            private ProtectedString DerefValue(string fieldName, PwEntry pe)
            {
                ProtectedString ps;

                string decrypted = pe.Strings.ReadSafe(fieldName);
                if (decrypted.IndexOf('{') >= 0)
                {
                    PwDatabase pd = m_host.MainWindow.DocumentManager.SafeFindContainerOf(pe);
                    SprContext ctx = new SprContext(pe, pd, (SprCompileFlags.Deref | SprCompileFlags.TextTransforms), false, false);
                    ps = new ProtectedString(true, (SprEngine.Compile(decrypted, ctx)));
                    return ps;
                }
                else
                {
                    ps = pe.Strings.GetSafe(fieldName);
                }

                return ps;
            }

        }

    }
}