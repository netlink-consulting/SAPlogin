﻿/*
  SAPlogin

  Copyright (c) 2019 Bernhard W. Radermacher <bernhard.raderamcher@netlink-consulting.com>
*/

using KeePassLib;

namespace SAPlogin
{
    public sealed class KeePassEntry
    {
        public readonly string SID = "";
        public readonly string Client = "";
        public readonly string Language = "";
        public readonly string Transaction = "";
        public readonly string Host = "";
        public readonly string Instance = "";

        public KeePassEntry(PwEntry pe, string DefaultLanguage)
        {
            string value = pe.Strings.ReadSafe("SAP_SID");
            if (!string.IsNullOrWhiteSpace(value)) SID = value.ToUpper();

            value = pe.Strings.ReadSafe("SAP_Client");
            if (!string.IsNullOrWhiteSpace(value)) Client = value;

            value = pe.Strings.ReadSafe("SAP_Language");
            if (!string.IsNullOrWhiteSpace(value)) Language = value.ToUpper();
            else Language = DefaultLanguage;

            value = pe.Strings.ReadSafe("SAP_Transaction");
            if (!string.IsNullOrWhiteSpace(value)) Transaction = value.ToUpper();

            value = pe.Strings.ReadSafe("SAP_Host");
            if (!string.IsNullOrWhiteSpace(value)) Host = value;

            value = pe.Strings.ReadSafe("SAP_Instance");
            if (!string.IsNullOrWhiteSpace(value)) Instance = value;
        }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(SID) &&
                   !string.IsNullOrEmpty(Client) &&
                   !(string.IsNullOrEmpty(Host) ^ string.IsNullOrEmpty(Instance));
        }

        public bool IsSAP()
        {
            return !string.IsNullOrEmpty(SID) ||
                   !string.IsNullOrEmpty(Client) ||
                   !string.IsNullOrEmpty(Transaction) ||
                   !string.IsNullOrEmpty(Host) ||
                   !string.IsNullOrEmpty(Instance);
        }
    }
}